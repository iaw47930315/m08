
@Component
public class ServiceClass {
	private final BeanFactory factory;
	private List<ProtoTypeBean> prototypeBeans;

	
	
	
	public ServiceClass(BeanFactory factory, List<ProtoTypeBean> prototypeBeans) {
		this.factory = factory;
		this.prototypeBeans = prototypeBeans;
	}

	@Autowired
	public ServiceClass(final BeanFactory f) {
		this.factory = f;
	}

	public void demoMethod(List<String> someArrayList) {
		this.prototypeBeans = someArrayList.stream().map(param -> factory.getBean(ProtoTypeBean.class, param))
				.collect(Collectors.toList());
	}

	public List<ProtoTypeBean> getPrototypeBeans() {
		return prototypeBeans;
	}
	
	

}

