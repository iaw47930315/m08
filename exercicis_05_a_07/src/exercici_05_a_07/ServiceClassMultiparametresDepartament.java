package exercici_05_a_07;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceClassMultiparametresDepartament {
	private final BeanFactory factory;
	private List<Departament> llistaDepartamentsMultiparametres;
	
	/**
	 * Constructor
	 */
	@Autowired
	public ServiceClassMultiparametresDepartament(final BeanFactory f) {
		this.factory = f;
	}

	public void demoMethod(List<Integer> llistaIdsDepartament, List<String> llistaNomDepartament) {
		this.llistaDepartamentsMultiparametres = llistaIdsDepartament.stream().map(param -> factory.getBean(Departament.class, param))
				.collect(Collectors.toList());	
		
		int index = 0;
		for (Departament dept : this.llistaDepartamentsMultiparametres) {
			dept.setNom(llistaNomDepartament.get(index));			
			index++;
		}
	}

	public List<Departament> getLlistaDepartamentsMultiparametres() {
		return llistaDepartamentsMultiparametres;
	}

}
