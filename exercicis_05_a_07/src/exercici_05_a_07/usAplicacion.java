package exercici_05_a_07;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class usAplicacion {
	
	/**
	 * Peronal
	 * @return
	 */
	public static List<Personal> inicialitzarPersonal() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);


		ServiceClassMultiparametresPersonal serviceClassMultiparametres = contexteAmbClasseConfig
				.getBean("serviceClassMultiparametresPersonal", ServiceClassMultiparametresPersonal.class);

		List<String> llistaDniPersonal = Arrays.asList("x10", "y20", "z30");
		List<String> llistaNomPersonal = Arrays.asList("tripulant_10", "tripulant_20", "tripulant_30");
		List<LocalDate> llistaDataCreacio = Arrays.asList(LocalDate.now(), LocalDate.now(), LocalDate.now());
		List<Integer> departament = Arrays.asList(1,2,3);
		serviceClassMultiparametres.demoMethod(llistaDniPersonal, llistaNomPersonal,llistaDataCreacio,departament);
		contexteAmbClasseConfig.close();
		return serviceClassMultiparametres.getLlistaPersonalMultiparametres();
	}

	
	/**
	 * Departament
	 * @return
	 */
	public static List<Departament> inicialitzarDepartaments() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);

		ServiceClassMultiparametresDepartament serviceClassMultiparametres = contexteAmbClasseConfig
				.getBean("serviceClassMultiparametresDepartament", ServiceClassMultiparametresDepartament.class);

		List<Integer> llistaIdsDepartament = Arrays.asList(1, 2, 3);
		List<String> llistaNomDepartament = Arrays.asList("dep-A", "dep-B", "dep-C");
		serviceClassMultiparametres.demoMethod(llistaIdsDepartament, llistaNomDepartament);
		contexteAmbClasseConfig.close();
		return serviceClassMultiparametres.getLlistaDepartamentsMultiparametres();
	}
}
