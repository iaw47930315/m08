<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!-- Perquè funcionin les form tags. -->

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Insertar tripulant</title>
	<!-- Carreguem un full d'estils css. -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/elMeuEstil.css">
</head>
<body>
	<h1>Hello vista</h1>
	<h1>Formulari per a insertar un tripulant</h1>
	
	<form:form action="procesarAltaTripulant" modelAttribute="elNouTripulant" method="POST">
		<table>
			<tr>
				<td> Nom del tripulant: </td>
				<td><form:input path="nom"/></td>
				<%-- L'atribut path serà = al nom de l'atribut/propietat del objecte 
				"elNouTripulant"      que és de tipus Tripulant. --%>
			<tr/>
			
			<tr>
				<td> Cognom del tripulant: </td>
				<td><form:input path="cognom"/></td>
			<tr/>
			<tr>
				<td> Email del tripulant: </td>
				<td><form:input path="email"/></td>
			<tr/>
			<tr>
				<td> Data alta del tripulant: </td>
				<td><!-- <form:input path="dataCreacio"/>  --></td>
			<tr/>
			<tr>
				<td> Departament (seleccionar Navegació perquè en la BD és l'únic que existeix): </td>
				<td><form:radiobutton path="departamentId" value="1" label="Navegació"/>
				<form:radiobutton path="departamentId" value="2" label="Infermeria"/>
				<form:radiobutton path="departamentId" value="3" label="Màquines"/>
				<form:radiobutton path="departamentId" value="4" label="Pont"/></td>
			<tr/>
			<tr>
				<td colspan="2"><input type="submit" value="Donar d'alta el tripulant"/></td>
			</tr>
		</table>
	</form:form>
</body>
</html>